{include file="header.tpl" title=$title}
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                Passwort ändern
            </div>
            <div class="panel-body">
                {validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>')}
                {if isset($error_form)}
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {$error_form}
                    </div>
                {/if}
                {if isset($success_form)}
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {$success_form}
                    </div>
                {/if}
                <form action="{site_url('user/settings')}" method="post" role="form">
                    <input type="hidden" name="{$csrf['token_name']}" value="{$csrf['token_hash']}">
                    <div class="form-group">
                        <label>Altes Passwort</label>
                        <input class="form-control" type="password" name="oldPassword" id="oldPassword" required>
                    </div>

                    <div class="form-group">
                        <label>Neues Passwort</label>
                        <input class="form-control" type="password" name="newPassword" id="newPassword" required>
                    </div>

                    <div class="form-group">
                        <label>Neues Passwort wiederholen</label>
                        <input class="form-control" type="password" name="cNewPassword" id="cNewPassword" required>
                    </div>

                    <input type="submit" class="btn btn-success" name="submit" id="submit" value="Passwort ändern">
                </form>
            </div>
        </div>
    </div>
</div>
{include file="footer.tpl"}