{include file="../header.tpl" title=$title}
<div class="panel panel-default">
    <div class="panel-heading">
        Klasse erstellen
    </div>
    <div class="panel-body">
        {validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>')}
        <form method="post">
            <input type="hidden" name="{$csrf['token_name']}" value="{$csrf['token_hash']}">
            <div class="form-group">
                <label>Name</label>
                <input type="text" class="form-control" id="name" name="name" maxlength="45">
            </div>

            <div class="form-group">
                <label>Beschreibung</label>
                <input type="text" class="form-control" id="description" name="description">
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" name="save" id="save" value="Absenden">
                <a class="btn btn-info" href="{site_url("teacher/class")}">Zurück</a>
            </div>
        </form>
    </div>
</div>
{include file="../footer.tpl"}