{include file="../header.tpl" title=$title}
{assign "class" $class[0]}
{assign "schoolYear" $schoolYear[0]}

<a class="btn btn-info" href="{site_url("teacher/class/list/{$class->id}")}">Zurück</a><br><br>

<div class="panel panel-default">
    <div class="panel-heading">
        Schüler Übersicht
    </div>
    <div class="panel panel-body">
        <table class="table table-striped" id="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Aktion</th>
            </tr>
            </thead>
            <tbody>
            {foreach $classMember as $var}
                <tr>
                    <td>{$var->id}</td>
                    <td>{$var->firstname} {$var->surename}</td>
                    <td>
                        <a href="{site_url("teacher/class/grades/overview/{$classId}/{$schoolYearId}/{$var->id}")}" class="fa fa-graduation-cap" data-toggle="tooltip" data-placement="bottom" title="Noten"></a>
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>
{include file="../footer.tpl"}