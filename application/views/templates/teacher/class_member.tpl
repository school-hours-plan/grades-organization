{include file="../header.tpl" title=$title}
{assign "class" $class[0]}

<a class="btn btn-info" href="{site_url("teacher/class")}">Zurück</a><br><br>

<div class="panel panel-default">
    <div class="panel-heading">
        Wähle ein Schuljahr
    </div>
    <div class="panel-body">
        <div class="list-group">
            {foreach $schoolyear as $var}
                <a class="list-group-item"
                   href="{site_url("teacher/class/list/{$class->id}/{$var->id}")}">
                    <span class="badge">{$classMemberCountBySchoolYear.{$var->id}}</span>
                    {$var->year}
                </a>
            {/foreach}
        </div>
    </div>
</div>
{include file="../footer.tpl"}