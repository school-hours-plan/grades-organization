{include file="../../header.tpl" title=$title}
{assign "grade" $grade[0]}

<div class="panel panel-default">
    <div class="panel-heading">
        Note löschen
    </div>
    <div class="panel-body">
        {validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>')}
        <form method="post">
            <input type="hidden" name="{$csrf['token_name']}" value="{$csrf['token_hash']}">
            <div class="form-group">
                <label>Möchtest du diese Note wirklich löschen?</label><br>
                <input type="checkbox" name="deleteAccept" id="deleteAccept"> Ja
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" name="save" id="save" value="Absenden">
                <a class="btn btn-info" href="{site_url("teacher/class/grades/overview/{$grade->classID}/{$grade->schoolyearID}/{$grade->accountID}")}">Zurück</a>
            </div>
        </form>
    </div>
</div>
{include file="../../footer.tpl"}