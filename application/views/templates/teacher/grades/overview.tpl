{include file="../../header.tpl" title=$title}
{assign "student" $student[0]}
{assign "class" $class[0]}
{assign "schoolYear" $schoolYear[0]}

<a class="btn btn-info" href="{site_url("teacher/class/list/{$class->id}/{$schoolYear->id}")}">Zurück</a><br><br>

<div class="panel panel-default">
    <div class="panel-heading">
        Noten Übersicht
    </div>
    <div class="panel-body">
        <a href="{site_url("teacher/class/grades/add/{$class->id}/{$schoolYear->id}/{$student->id}")}" class="btn btn-success">Note Erstellen</a><br><br>
        {if isset($error_form)}
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {$error_form}
            </div>
        {/if}
        {if isset($success_form)}
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {$success_form}
            </div>
        {/if}
        <table class="table table-striped" id="table">
            <thead>
            <tr>
                <th>Fach</th>
                <th>Note</th>
                <th>Eintragungsdatum</th>
                <th>Lehrer</th>
                <th>Aktionen</th>
            </tr>
            </thead>
            <tbody>
            {foreach $grades as $var}
                <tr>
                    <td>{$var->subjectName} ({$var->subjectShortname})</td>
                    <td>{$var->gradeName}</td>
                    <td>{$var->timestamp|date_format:"%d.%m.%G %H:%M:%S"}</td>
                    <td>{$var->teacherFirstname|substr:0:1}. {$var->teacherSurename}</td>
                    <td>
                        <a href="{site_url("teacher/class/grades/edit/{$var->id}")}" class="fa fa-pencil" data-toggle="tooltip" data-placement="bottom" title="Note bearbeiten"></a>
                        <a href="{site_url("teacher/class/grades/delete/{$var->id}")}" class="fa fa-trash" data-toggle="tooltip" data-placement="bottom" title="Note löschen"></a>
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>
{include file="../../footer.tpl"}