{include file="../../header.tpl" title=$title}
{assign "grade" $grade[0]}

<div class="panel panel-default">
    <div class="panel-heading">
        Note bearbeiten
    </div>
    <div class="panel-body">
        {validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>')}
        <form method="post">
            <input type="hidden" name="{$csrf['token_name']}" value="{$csrf['token_hash']}">
            <div class="form-group">
                <label>Schüler</label>
                <select class="form-control" id="student" name="student" disabled>
                    <option value="0">Schüler auswählen</option>
                    {foreach $students as $var}
                        <option value="{$var->id}" {if $var->id eq $grade->accountID}selected{/if}>{$var->firstname} {$var->surename}</option>
                    {/foreach}
                </select>
            </div>

            <div class="form-group">
                <label>Klasse</label>
                <select class="form-control" id="class" name="class" disabled>
                    <option value="0">Klasse auswählen</option>
                    {foreach $classes as $var}
                        <option value="{$var->id}" {if $var->id eq $grade->classID}selected{/if}>{$var->name}</option>
                    {/foreach}
                </select>
            </div>

            <div class="form-group">
                <label>Schuljahr</label>
                <select class="form-control" id="schoolYear" name="schoolYear" disabled>
                    <option value="0">Schuljahr auswählen</option>
                    {foreach $schoolYears as $var}
                        <option value="{$var->id}" {if $var->id eq $grade->schoolyearID}selected{/if}>{$var->year}</option>
                    {/foreach}
                </select>
            </div>

            <div class="form-group">
                <label>Fach</label>
                <select class="form-control" id="subject" name="subject">
                    <option value="0">Fach auswählen</option>
                    {foreach $subjects as $var}
                        <option value="{$var->id}" {if $var->id eq $grade->subjectID}selected{/if}>{$var->name} ({$var->shortname})</option>
                    {/foreach}
                </select>
            </div>

            <div class="form-group">
                <label>Note</label>
                <select class="form-control" name="grade" id="grade">
                    <option value="0">Note auswählen</option>
                    {foreach $gradesValue AS $var}
                        <option value="{$var->id}" {if $var->id eq $grade->gradeID}selected{/if}>{$var->name} ({$var->number})</option>
                    {/foreach}
                </select>
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" name="save" id="save" value="Absenden">
                <a class="btn btn-info" href="{site_url("teacher/class/grades/overview/{$grade->classID}/{$grade->schoolyearID}/{$grade->accountID}")}">Zurück</a>
            </div>
        </form>
    </div>
</div>
{include file="../../footer.tpl"}