{include file="../header.tpl" title=$title}
<div class="panel panel-default">
    <div class="panel-heading">
        Fächer Übersicht
    </div>
    <div class="panel-body">
        {if isset($error_form)}
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {$error_form}
            </div>
        {/if}
        {if isset($success_form)}
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {$success_form}
            </div>
        {/if}
        <a class="btn btn-success" href="{site_url("admin/subject/add")}">Hinzufügen</a><br><br>
        <table class="table table-striped" id="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Kurzname</th>
                <th>Aktion</th>
            </tr>
            </thead>
            <tbody>
            {foreach $subject as $var}
                <tr>
                    <td>{$var->name}</td>
                    <td>{$var->shortname}</td>
                    <td>
                        <a class="fa fa-pencil" href="{site_url("admin/subject/edit/{$var->id}")}"></a>
                        <a class="fa fa-trash" href="{site_url("admin/subject/delete/{$var->id}")}"></a>
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>
{include file="../footer.tpl"}