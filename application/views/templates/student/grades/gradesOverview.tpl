{include file="../../header.tpl" title=$title}
{assign "class" $class[0]}
{assign "schoolYear" $schoolYear[0]}

<a class="btn btn-info" href="{site_url("student/grades/{$class->id}")}">Zurück</a><br><br>

<div class="panel panel-default">
    <div class="panel-heading">
        Noten Übersicht "{$class->name} - {$schoolYear->year}"
    </div>
    <div class="panel-body">
        <table class="table table-striped" id="table">
            <thead>
            <tr>
                <th>Fach</th>
                <th>Note</th>
                <th>Eintragungsdatum</th>
                <th>Lehrer</th>
            </tr>
            </thead>
            {foreach $grades as $var}
                <tr>
                    <td>{$var->subjectName} ({$var->subjectShortname})</td>
                    <td>{$var->gradeName}</td>
                    <td>{$var->timestamp|date_format:"%d.%m.%G %H:%M:%S"}</td>
                    <td>{$var->teacherFirstname|substr:0:1}. {$var->teacherSurename}</td>
                </tr>
            {/foreach}
        </table>
    </div>
</div>
{include file="../../footer.tpl"}