{include file="../../header.tpl" title=$title}
<div class="panel panel-default">
    <div class="panel-heading">
        Wähle eine Klasse
    </div>
    <div class="panel-body">
        <div class="list-group">
            {foreach $classes as $var}
                <a class="list-group-item"
                   href="{site_url("student/grades/{$var->classID}")}">
                    {$var->name} {$var->description}
                </a>
            {/foreach}
        </div>
    </div>
</div>
{include file="../../footer.tpl"}