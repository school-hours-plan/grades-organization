{include file="../header.tpl" title=$title}
{assign "user" $user[0]}
<div class="panel panel-default">
    <div class="panel-heading">
        Account "{$user->firstname} {$user->surename}" löschen
    </div>
    <div class="panel-body">
        {validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>')}
        <form method="post">
            <input type="hidden" name="{$csrf['token_name']}" value="{$csrf['token_hash']}">
            <div class="form-group">
                <label>Zum löschen des Benutzers geben Sie den Vor- und Nachnamen als Secret ein</label>
                <input type="text" class="form-control" id="secret" name="secret">
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" name="delete" id="delete" value="Löschen">
                <a class="btn btn-info" href="{site_url("{$page}")}">Zurück</a>
            </div>
        </form>
    </div>
</div>
{include file="../footer.tpl"}