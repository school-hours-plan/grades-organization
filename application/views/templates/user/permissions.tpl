{include file="../header.tpl" title=$title}
{assign "user" $user[0]}

<div class="panel panel-default">
    <div class="panel-heading">
        Zugangsberechtigungen von {$user->firstname} {$user->surename}
    </div>
    <div class="panel-body">
        {validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>')}
        <form method="post">
            <input type="hidden" name="{$csrf['token_name']}" value="{$csrf['token_hash']}">
            <b>Der Account hat folgende Zugangsberechtigungen:</b>
            <div class="form-group">
                {foreach $allPermissions as $var}
                    <div class="input-group">
                        <span class="input-group-addon">
                            <input type="checkbox" name="{$var->permission_name}" id="{$var->permission_name}" value="{$var->permission_name}" {if $var->permission_name|in_array:$permissionsSimple}checked{/if}>
                        </span>
                        <input type="text" class="form-control" value="{$var->permission_desc}" disabled>
                    </div>
                {/foreach}
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" name="submit" id="submit" value="Absenden">
                <a class="btn btn-info" href="{site_url("{$page}")}">Zurück</a>
            </div>
        </form>
    </div>
</div>
{include file="../footer.tpl"}