{include file="../header.tpl" title=$title}
<div class="panel panel-default">
    <div class="panel-heading">
        {$subtitle}
    </div>
    <div class="panel-body">
        {if isset($error_form)}
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {$error_form}
            </div>
        {/if}
        {if isset($success_form)}
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {$success_form}
            </div>
        {/if}
        <a class="btn btn-success" href="{site_url("{$page}/add")}">Hinzufügen</a><br><br>

        <table class="table table-striped" id="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>E-Mail</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {foreach $accounts as $var}
                <tr>
                    <td>{$var->firstname} {$var->surename}</td>
                    <td>{$var->email}</td>
                    <td>
                        <a class="fa fa-pencil" href="{site_url("{$page}/edit/{$var->id}")}" data-toggle="tooltip"
                           data-placement="bottom" title="Bearbeiten"></a>
                        <a class="fa fa-trash" href="{site_url("{$page}/delete/{$var->id}")}" data-toggle="tooltip"
                           data-placement="bottom" title="Löschen"></a>
                        {if 'teacher'|in_array:$permissions AND $page eq "teacher/students"}
                            <a class="fa fa-graduation-cap" href="{site_url("teacher/students/class/{$var->id}")}"
                               data-toggle="tooltip" data-placement="bottom" title="Klassen Verwaltung"></a>
                        {/if}
                        {if 'admin'|in_array:$permissions}
                            <a class="fa fa-drivers-license" href="{site_url("admin/accounts/permission/{$var->id}")}"
                               data-toggle="tooltip" data-placement="bottom" title="Zugangsberechtigungen"></a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>
{include file="../footer.tpl"}