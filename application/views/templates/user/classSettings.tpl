{include file="../header.tpl" title=$title}
{assign "user" $user[0]}

{if isset($add)}
    <div class="panel panel-default">
        <div class="panel-heading">
            Klassenzuweisung hinzufügen
        </div>
        <div class="panel-body">
            {validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>')}
            <form method="post">
                <input type="hidden" name="{$csrf['token_name']}" value="{$csrf['token_hash']}">
                <div class="form-group">
                    <label>Schüler</label>

                    <select class="form-control" disabled>
                        <option value="0">Bitte Schüler auswählen</option>
                        {foreach $studentsDropdown as $var}
                            <option value="{$var->id}"
                                    {if $var->id eq $user->id}selected{/if}>{$var->firstname} {$var->surename}</option>
                        {/foreach}
                    </select>
                </div>

                <div class="form-group">
                    <label>Klasse</label>

                    <select id="class" name="class" class="form-control">
                        <option value="0">Bitte Klasse auswählen</option>
                        {foreach $classesDropdown as $var}
                            <option value="{$var->id}">{$var->name}</option>
                        {/foreach}
                    </select>
                </div>

                <div class="form-group">
                    <label>Schuljahr</label>

                    <select id="schoolYear" name="schoolYear" class="form-control">
                        <option value="0">Bitte Schuljahr auswählen</option>
                        {foreach $schoolYearsDropdown as $var}
                            <option value="{$var->id}">{$var->year}</option>
                        {/foreach}
                    </select>
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-success" name="submit" id="submit" value="Absenden">
                    <a href="{site_url("{$page}/class/{$user->id}")}" class="btn btn-danger">Abbrechen</a>
                </div>
            </form>
        </div>
    </div>
{/if}

{if isset($class)}
    {assign "class" $class[0]}
    <div class="panel panel-default">
        <div class="panel-heading">
            Klassenzuweisung löschen
        </div>
        <div class="panel-body">
            {validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>')}
            <form method="post">
                <input type="hidden" name="{$csrf['token_name']}" value="{$csrf['token_hash']}">
                <div class="form-group">
                    <label>Möchtest du die Klassenzuweisung "{$user->firstname} {$user->surename} - {$class->name}
                        - {$class->year}" wirklich löschen?</label><br>
                    <input type="checkbox" name="sure" id="sure" value="yes"> Ja
                </div>

                <div class="form-group">
                    <input type="submit" class="btn btn-success" name="submit" id="submit" value="Absenden">
                    <a href="{site_url("{$page}/class/{$user->id}")}" class="btn btn-danger">Abbrechen</a>
                </div>
            </form>
        </div>
    </div>
{/if}

<div class="panel panel-default">
    <div class="panel-heading">
        Klassenzuweisung von {$user->firstname} {$user->surename}
    </div>
    <div class="panel-body">
        {if isset($error_form)}
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {$error_form}
            </div>
            <br>
            <br>
        {/if}
        {if isset($success_form)}
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {$success_form}
            </div>
            <br>
            <br>
        {/if}
        {if !isset($add)}
            <a href="{site_url("{$page}/class/{$user->id}/add")}" class="btn btn-success">Hinzufügen</a>
        {/if}
        <a class="btn btn-info" href="{site_url("teacher/students")}">Zurück</a><br><br>

        <table class="table table-striped" id="table">
            <thead>
            <tr>
                <th>Klasse</th>
                <th>Schuljahr</th>
                <th>Aktion</th>
            </tr>
            </thead>
            <tbody>
            {foreach $classes as $var}
                <tr>
                    <td>{$var->name} - {$var->description}</td>
                    <td>{$var->year}</td>
                    <td>
                        <a class="fa fa-trash"
                           href="{site_url("teacher/students/class/{$user->id}/delete/{$var->classID}/{$var->schoolyearID}")}"
                           data-toggle="tooltip" data-placement="bottom" title="Löschen"></a>
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>
{include file="../footer.tpl"}