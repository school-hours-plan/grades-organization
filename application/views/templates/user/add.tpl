{include file="../header.tpl" title=$title}
<div class="panel panel-default">
    <div class="panel-heading">
        Account hinzufügen
    </div>
    <div class="panel-body">
        {validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>')}
        <form method="post">
            <input type="hidden" name="{$csrf['token_name']}" value="{$csrf['token_hash']}">
            <div class="form-group">
                <label>Vorname</label>
                <input type="text" class="form-control" id="firstname" name="firstname">
            </div>

            <div class="form-group">
                <label>Nachname</label>
                <input type="text" class="form-control" id="surename" name="surename">
            </div>

            <div class="form-group">
                <label>E-Mail</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>

            <div class="form-group">
                <label>Passwort</label>
                <input type="password" class="form-control" id="newPassword" name="newPassword">
            </div>

            <div class="form-group">
                <label>Passwort wiederholen</label>
                <input type="password" class="form-control" id="cNewPassword" name="cNewPassword">
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-success" name="save" id="save" value="Absenden">
                <a class="btn btn-info" href="{site_url("{$page}")}">Zurück</a>
            </div>
        </form>
    </div>
</div>
{include file="../footer.tpl"}