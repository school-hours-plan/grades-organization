{include file="header.tpl" title=$title}
{if 'admin'|in_array:$permissions}
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-coffee fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{$teacherCount}</div>
                            <div>Lehrer existieren!</div>
                        </div>
                    </div>
                </div>
                <a href="{site_url("admin/teacher")}">
                    <div class="panel-footer">
                        <span class="pull-left">Übersicht</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user-circle fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{$accountCount}</div>
                            <div>Accounts existieren insgesamt!</div>
                        </div>
                    </div>
                </div>
                <a href="{site_url("admin/accounts")}">
                    <div class="panel-footer">
                        <span class="pull-left">Übersicht</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-graduation-cap fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{$subjectCount}</div>
                            <div>Fächer existieren!</div>
                        </div>
                    </div>
                </div>
                <a href="{site_url("admin/subject")}">
                    <div class="panel-footer">
                        <span class="pull-left">Übersicht</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
{/if}
{if 'teacher'|in_array:$permissions}
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-graduation-cap fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{$studentCount}</div>
                            <div>Schüler existieren!</div>
                        </div>
                    </div>
                </div>
                <a href="{site_url("teacher/students")}">
                    <div class="panel-footer">
                        <span class="pull-left">Übersicht</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-clipboard fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{$classCount}</div>
                            <div>Klassen existieren!</div>
                        </div>
                    </div>
                </div>
                <a href="{site_url("teacher/class")}">
                    <div class="panel-footer">
                        <span class="pull-left">Übersicht</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-newspaper-o fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{$gradeCount}</div>
                            <div>Noten wurden bisher eingetragen!</div>
                        </div>
                    </div>
                </div>
                <a href="{site_url("teacher/class")}">
                    <div class="panel-footer">
                        <span class="pull-left">Übersicht</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
{/if}
{if 'student'|in_array:$permissions}
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-newspaper-o fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{$personallyGradeCount}</div>
                            <div>Noten wurden dir über alle Schuljahre hinweg bereits gegeben!</div>
                        </div>
                    </div>
                </div>
                <a href="{site_url("student/grades")}">
                    <div class="panel-footer">
                        <span class="pull-left">Übersicht</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-sort-numeric-asc fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{$personallyGradeAverage}</div>
                            <div>Dein Notendurchschnitt aller dir bereits gegebenen Noten!</div>
                        </div>
                    </div>
                </div>
                <a href="{site_url("student/grades")}">
                    <div class="panel-footer">
                        <span class="pull-left">Übersicht</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
{/if}
{include file="footer.tpl"}