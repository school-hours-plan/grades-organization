{include file="header.tpl" login="true" title="Login"}
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Einloggen</h3>
                </div>
                <div class="panel-body">
                    {validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>')}
                    {if isset($error)}
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {$error}
                        </div>
                    {/if}
                    {if isset($success)}
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {$success}
                        </div>
                    {/if}
                    <form action="{site_url('login/loginMe')}" method="post" role="form">
                        <input type="hidden" name="{$csrf['token_name']}" value="{$csrf['token_hash']}">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Passwort" name="password" type="password"
                                       value="">
                            </div>
                            <input type="submit" class="btn btn-lg btn-success btn-block" name="submit" id="submit" value="Login">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{include file="footer.tpl" login="true"}