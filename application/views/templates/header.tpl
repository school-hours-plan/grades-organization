<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Grades Management - {$title}</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/metisMenu.min.css" rel="stylesheet">
    <link href="/assets/css/sb-admin-2.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- CDN -->
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css" rel="stylesheet";
    <!-- CDN End -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
{if !isset($login)}
<div id="wrapper">
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{base_url()}">Grades Management</a>
        </div>

        <ul class="nav navbar-top-links navbar-right">
            <button class="btn btn-info" data-toggle="modal" data-target="#creditsModal">Credits</button>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> {$firstname} {$surename} <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href="{site_url('user/settings')}"><i class="fa fa-gear fa-fw"></i> Einstellungen</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="{site_url('user/logout')}"><i class="fa fa-sign-out fa-fw"></i> Ausloggen</a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="{site_url('dashboard')}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    {if 'student'|in_array:$permissions}
                        <li>
                            <a href="#"><i class="fa fa-graduation-cap fa-fw"></i> Schüler<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{site_url('student/grades')}">Noten Übersicht</a>
                                </li>
                            </ul>
                        </li>
                    {/if}
                    {if 'teacher'|in_array:$permissions}
                    <li>
                        <a href="#"><i class="fa fa-user-circle-o fa-fw"></i> Lehrer<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{site_url('teacher/students')}">Schüler Account Verwaltung</a>
                                <a href="{site_url('teacher/class')}">Klassen Übersicht</a>
                            </li>
                        </ul>
                    </li>
                    {/if}
                    {if 'admin'|in_array:$permissions}
                    <li>
                        <a href="#"><i class="fa fa-wrench fa-fw"></i> Administrator<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{site_url('admin/teacher')}">Lehrer Account Verwaltung</a>
                                <a href="{site_url('admin/accounts')}">Account Verwaltung</a>
                                <a href="{site_url('admin/subject')}">Fächer Verwaltung</a>
                            </li>
                        </ul>
                    </li>
                    {/if}
                </ul>
            </div>
        </div>
    </nav>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{$title}</h1>
            </div>
        </div>
{/if}