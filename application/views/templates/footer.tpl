{if !isset($login)}
    </div>
    </div>
{/if}
<!-- Credits Modal -->
<div class="modal fade" id="creditsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Credits</h4>
            </div>
            <div class="modal-body">
                Contributors:<br>
                <ul>
                    <li><a href="http://elyday.net">Lars R. (elyday)</a></li>
                    <li><a href="https://github.com/Burnett01">Steven A. (burnett01)</a></li>
                </ul>
                Used Software:<br>
                <ul>
                    <li><a href="https://codeigniter.com/">Codeigniter 3.1.5</a></li>
                    <li><a href="http://www.smarty.net/">Smarty 3.1.30</a></li>
                    <li><a href="http://getbootstrap.com/">Bootstrap 3</a></li>
                    <li><a href="https://startbootstrap.com/template-overviews/sb-admin-2/">SB Admin 2</a></li>
                </ul>
                Built with <i class="fa fa-heart" style="color: red;"></i> in Viersen and Aachen, Germany.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- JS -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/metisMenu.min.js"></script>
<script src="/assets/js/sb-admin-2.js"></script>

<!-- CDN -->
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
<!-- CDN End -->

<script type="application/javascript">
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('#table').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
            }
        });
    });
</script>
</body>
</html>