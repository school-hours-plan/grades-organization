<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* CUSTOM ROUTES */
$route['dashboard'] = 'user';

$route['admin/accounts'] = 'account/overview';
$route['admin/accounts/add'] = 'account/create';
$route['admin/accounts/edit/(:num)'] = 'account/edit/$1';
$route['admin/accounts/delete/(:num)'] = 'account/delete/$1';
$route['admin/accounts/permission/(:num)'] = 'account/permission/$1';

$route['admin/teacher/add'] = 'admin/teacher_add';
$route['admin/teacher/edit/(:num)'] = 'admin/teacher_edit/$1';
$route['admin/teacher/delete/(:num)'] = 'admin/teacher_delete/$1';

$route['admin/subject'] = 'admin/subjectList';
$route['admin/subject/add'] = 'admin/subjectAdd';
$route['admin/subject/edit/(:num)'] = 'admin/subjectEdit/$1';
$route['admin/subject/delete/(:num)'] = 'admin/subjectDelete/$1';

$route['teacher/class'] = 'classManagement/overview';
$route['teacher/class/add'] = 'classManagement/create';
$route['teacher/class/edit/(:num)'] = 'classManagement/edit/$1';
$route['teacher/class/delete/(:num)'] = 'classManagement/delete/$1';
$route['teacher/class/list/(:num)'] = 'classManagement/classSchoolYearOverview/$1';
$route['teacher/class/list/(:num)/(:num)'] = 'classManagement/classMemberListWithSchoolYear/$1/$2';

$route['teacher/class/grades/overview/(:num)/(:num)/(:num)'] = 'teacherGrades/gradesOverview/$1/$2/$3';
$route['teacher/class/grades/add/(:num)/(:num)/(:num)'] = 'teacherGrades/gradesAdd/$1/$2/$3';
$route['teacher/class/grades/edit/(:num)'] = 'teacherGrades/gradesEdit/$1';
$route['teacher/class/grades/delete/(:num)'] = 'teacherGrades/gradesDelete/$1';

$route['teacher/students'] = 'studentAccounts/overview';
$route['teacher/students/add'] = 'studentAccounts/create';
$route['teacher/students/class/(:num)'] = 'studentAccounts/classSettings/$1';
$route['teacher/students/class/(:num)/add'] = 'studentAccounts/classSettingsAdd/$1';
$route['teacher/students/class/(:num)/delete/(:num)/(:num)'] = 'studentAccounts/classSettings/$1/$2/$3';
$route['teacher/students/edit/(:num)'] = 'studentAccounts/edit/$1';
$route['teacher/students/delete/(:num)'] = 'studentAccounts/delete/$1';

$route['student/grades'] = 'student/classOverview';
$route['student/grades/(:num)'] = 'student/schoolYearOverview/$1';
$route['student/grades/(:num)/(:num)'] = 'student/gradesOverview/$1/$2';