<?php

/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 11.06.2017
 * Time: 18:05
 */
class Account extends Base_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Subject_model');
        $this->load->model('Permission_model');
        $this->isLoggedIn();
        $this->hasAccess('admin');
        $this->load->library('form_validation');
    }

    function overview()
    {
        $this->global['title'] = 'Account Verwaltung';
        $this->global['subtitle'] = 'Accounts';
        $this->global['page'] = 'admin/accounts';
        $this->global['accounts'] = $this->User_model->getAllAccounts();

        $this->smarty->view('user/overview.tpl', $this->global);
    }

    function create()
    {
        $this->global['title'] = 'Account Verwaltung - Hinzufügen';
        $this->global['page'] = 'admin/accounts';

        $this->form_validation->set_rules('firstname', 'Vorname', 'required|max_length[20]');
        $this->form_validation->set_rules('surename', 'Nachname', 'required|max_length[20]');
        $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
        $this->form_validation->set_rules('newPassword', 'Passwort', 'required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword', 'Passwort wiederholen', 'required|matches[newPassword]|max_length[20]');

        if (!$this->form_validation->run()) {
            $this->smarty->view('user/add.tpl', $this->global);
        } else {
            $firstName = $this->input->post('firstname');
            $sureName = $this->input->post('surename');
            $email = $this->input->post('email');
            $newPassword = $this->input->post('newPassword');

            $userData = array(
                "email" => $email,
                "password" => getHashedPassword($newPassword),
                "firstname" => $firstName,
                "surename" => $sureName
            );

            $result = $this->User_model->addNewUser($userData);

            if ($result > 0) {
                $this->session->set_flashdata('success_form', "Account erfolgreich hinzugefügt. <b>Der neue Account hat noch keinerlei Rechte! Du musst diese nun dem User noch zuweisen!</b>");
            } else {
                $this->session->set_flashdata('error_form', 'Account konnte nicht hinzugefügt werden.');
            }

            redirect("admin/accounts");
        }
    }

    function edit($acocuntId)
    {
        $this->global['title'] = 'Account Verwaltung - Bearbeiten';
        $this->global['user'] = $this->User_model->getUserInfo($acocuntId);
        $this->global['page'] = 'admin/accounts';

        $this->form_validation->set_rules('firstname', 'Vorname', 'required|max_length[20]');
        $this->form_validation->set_rules('surename', 'Nachname', 'required|max_length[20]');
        $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
        $this->form_validation->set_rules('newPassword', 'Neues Passwort', 'max_length[20]');
        $this->form_validation->set_rules('cNewPassword', 'Neues Passwort wiederholen', 'matches[newPassword]|max_length[20]');

        if (!$this->form_validation->run()) {
            $this->smarty->view('user/edit.tpl', $this->global);
        } else {
            $firstname = $this->input->post('firstname');
            $surename = $this->input->post('surename');
            $email = $this->input->post('email');
            $newPassword = $this->input->post('newPassword');

            $userData = array(
                "email" => $email,
                "firstname" => $firstname,
                "surename" => $surename
            );


            $result = $this->User_model->changeUserData($acocuntId, $userData);

            if ($newPassword != "") {
                $userData = array(
                    "password" => getHashedPassword($newPassword)
                );
                $password = $this->User_model->changeUserData($acocuntId, $userData);
            }

            if ($result > 0 || isset($password)) {
                if (isset($password)) {
                    $this->session->set_flashdata('success_form', 'Account erfolgreich aktualisiert. Das Passwort wurde geändert!');
                } else {
                    $this->session->set_flashdata('success_form', 'Account erfolgreich aktualisiert.');
                }
            } else {
                $this->session->set_flashdata('error_form', 'Account konnte nicht aktualisiert werden oder es wurden keine Änderung vorgenommen.');
            }

            redirect("admin/accounts");
        }
    }

    function delete($accountId)
    {
        $this->global['title'] = 'Account Verwaltung - Löschen';
        $this->global['user'] = $this->User_model->getUserInfo($accountId);
        $this->global['page'] = 'admin/accounts';

        $this->form_validation->set_rules('secret', 'Secret', 'required');

        if (!$this->form_validation->run()) {
            $this->smarty->view('user/delete.tpl', $this->global);
        } else {
            $secret = $this->input->post('secret');
            $fullName = $this->global['user'][0]->firstname . ' ' . $this->global['user'][0]->surename;

            if ($secret == $fullName) {
                $result = $this->User_model->deleteUser($accountId);
                if ($result > 0) {
                    $this->session->set_flashdata('success_form', 'Account erfolgreich gelöscht.');
                } else {
                    $this->session->set_flashdata('error_form', 'Account konnte nicht gelöscht werden.');
                }
            } else {
                $this->session->set_flashdata('error_form', 'Der eingegebene Secret ist falsch.');
            }

            redirect("admin/accounts");
        }
    }

    function permission($accountId)
    {
        $this->global['title'] = 'Account Verwaltung - Zugangsberechtigungen';
        $this->global['user'] = $this->User_model->getUserInfo($accountId);
        $this->global['allPermissions'] = $this->Permission_model->getAllPermission();
        $this->global['permissionsSimple'] = $this->Permission_model->getSimpleAccountPermissionArray($accountId);
        $this->global['page'] = 'admin/accounts';

        foreach ($this->global['allPermissions'] as $var) {
            $this->form_validation->set_checkbox($var->permission_name, $var->permission_name);
            $this->form_validation->set_rules($var->permission_name, $var->permission_desc, 'equalsToString[' . $var->permission_name . ']');
        }

        if (!$this->form_validation->run()) {
            $this->smarty->view('user/permissions.tpl', $this->global);
        } else {
            $result = 0;
            $this->Permission_model->removeAllAccountPermissions($accountId);

            foreach ($this->global['allPermissions'] as $var) {
                $input = $this->input->post($var->permission_name);

                if(isset($input)) {
                    $permissionData = array(
                        "accountID" => $accountId,
                        "permissionID" => $var->id
                    );
                    $result = $this->Permission_model->addAccountPermission($permissionData);
                }
            }

            if ($result > 0) {
                $this->session->set_flashdata('success_form', 'Die Zugangsberechtigungen des Accounts wurden erfolgreich bearbeitet.');
            } else {
                $this->session->set_flashdata('error_form', 'Die Zugangsberechtigungen konnten nicht bearbeitet werden.');
            }

            redirect('admin/accounts');
        }
    }
}