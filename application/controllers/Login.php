<?php

class Login extends Base_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Login_model');
    }

    function index()
    {
        $this->isLoggedIn();
    }

    function isLoggedIn()
    {
        $isLoggedIn = $this->session->userdata('isLoggedIn');
        $error = $this->session->flashdata('error');
        $success = $this->session->flashdata('success');

        $this->global["error"] = $error;
        $this->global["success"] = $success;

        if (!isset($isLoggedIn) || $isLoggedIn != true) {
            $this->smarty->view('login.tpl', $this->global);
        } else {
            redirect('/dashboard');
        }
    }

    function loginMe()
    {
        $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email|max_length[128]|xss_clean|trim');
        $this->form_validation->set_rules('password', 'Passwort', 'required|max_length[32]');

        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $result = $this->Login_model->loginMe($email, $password);

            if (count($result) > 0) {
                foreach ($result as $res) {
                    $sessionArray = array(
                        "userId" => $res->id,
                        "email" => $res->email,
                        "firstname" => $res->firstname,
                        "surename" => $res->surename,
                        "isLoggedIn" => true
                    );

                    $this->session->set_userdata($sessionArray);
                    redirect('/dashboard');
                }
            } else {
                $this->session->set_flashdata('error', 'E-Mail oder Passwort ist falsch!');
                redirect('/login');
            }
        }
    }
}