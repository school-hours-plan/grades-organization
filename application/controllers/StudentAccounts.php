<?php

/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 11.06.2017
 * Time: 17:26
 */
class StudentAccounts extends Base_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Class_model');
        $this->load->model('Schoolyear_model');
        $this->isLoggedIn();
        $this->hasAccess('teacher');
        $this->load->library('form_validation');
    }

    function overview()
    {
        $this->global['title'] = 'Schüler Account Verwaltung - Übersicht';
        $this->global['subtitle'] = 'Schüler Accounts';
        $this->global['page'] = 'teacher/students';
        $this->global['accounts'] = $this->User_model->getStudentAccounts();

        $this->smarty->view('user/overview.tpl', $this->global);
    }

    function create()
    {
        $this->global['title'] = 'Schüler Account Verwaltung - Hinzufügen';
        $this->global['page'] = 'teacher/students';

        $this->form_validation->set_rules('firstname', 'Vorname', 'required|max_length[20]');
        $this->form_validation->set_rules('surename', 'Nachname', 'required|max_length[20]');
        $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
        $this->form_validation->set_rules('newPassword', 'Passwort', 'required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword', 'Passwort wiederholen', 'required|matches[newPassword]|max_length[20]');

        if (!$this->form_validation->run()) {
            $this->smarty->view('user/add.tpl', $this->global);
        } else {
            $firstName = $this->input->post('firstname');
            $sureName = $this->input->post('surename');
            $email = $this->input->post('email');
            $newPassword = $this->input->post('newPassword');

            $userData = array(
                "email" => $email,
                "password" => getHashedPassword($newPassword),
                "firstname" => $firstName,
                "surename" => $sureName
            );

            $result = $this->User_model->addNewUser($userData, 1);

            if ($result > 0 || isset($password)) {
                $this->session->set_flashdata('success_form', 'Schüler erfolgreich hinzugefügt.');
            } else {
                $this->session->set_flashdata('error_form', 'Schüler konnte nicht hinzugefügt werden.');
            }

            redirect('teacher/students');
        }
    }

    function edit($studentId)
    {
        $this->global['title'] = 'Schüler Account Verwaltung - Bearbeiten';
        $this->global['user'] = $this->User_model->getUserInfo($studentId);
        $this->global['page'] = 'teacher/students';

        $this->form_validation->set_rules('firstname', 'Vorname', 'required|max_length[20]');
        $this->form_validation->set_rules('surename', 'Nachname', 'required|max_length[20]');
        $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
        $this->form_validation->set_rules('newPassword', 'Neues Passwort', 'max_length[20]');
        $this->form_validation->set_rules('cNewPassword', 'Neues Passwort wiederholen', 'matches[newPassword]|max_length[20]');

        if (!$this->form_validation->run()) {
            $this->smarty->view('user/edit.tpl', $this->global);
        } else {
            $firstname = $this->input->post('firstname');
            $surename = $this->input->post('surename');
            $email = $this->input->post('email');
            $newPassword = $this->input->post('newPassword');

            $userData = array(
                "email" => $email,
                "firstname" => $firstname,
                "surename" => $surename
            );


            $result = $this->User_model->changeUserData($studentId, $userData);

            if ($newPassword != "") {
                $userData = array(
                    "password" => getHashedPassword($newPassword)
                );
                $password = $this->User_model->changeUserData($studentId, $userData);
            }

            if ($result > 0 || isset($password)) {
                if (isset($password)) {
                    $this->session->set_flashdata('success_form', 'Schüler erfolgreich aktualisiert. Das Passwort wurde geändert!');
                } else {
                    $this->session->set_flashdata('success_form', 'Schüler erfolgreich aktualisiert.');
                }
            } else {
                $this->session->set_flashdata('error_form', 'Schüler konnte nicht aktualisiert werden oder es wurden keine Änderung vorgenommen.');
            }

            redirect('teacher/students');
        }
    }

    function delete($studentId)
    {
        $this->global['title'] = 'Schüler Account Verwaltung - Löschen';
        $this->global['user'] = $this->User_model->getUserInfo($studentId);
        $this->global['page'] = 'teacher/students';

        $this->form_validation->set_rules('secret', 'Secret', 'required');

        if (!$this->form_validation->run()) {
            $this->smarty->view('user/delete.tpl', $this->global);
        } else {
            $secret = $this->input->post('secret');
            $fullName = $this->global['user'][0]->firstname . ' ' . $this->global['user'][0]->surename;

            if ($secret == $fullName) {
                $result = $this->User_model->deleteUser($studentId);
                if ($result > 0) {
                    $this->session->set_flashdata('success_form', 'Schüler erfolgreich gelöscht.');
                } else {
                    $this->session->set_flashdata('error_form', 'Schüler konnte nicht gelöscht werden.');
                }
            } else {
                $this->session->set_flashdata('error_form', 'Der eingegebene Secret ist falsch.');
            }

            redirect('teacher/students');
        }
    }

    function classSettings($studentId, $classId = null, $schoolYearId = null)
    {
        $this->global['title'] = 'Schüler Account Verwaltung - Klassenzuweisung';
        $this->global['user'] = $this->User_model->getUserInfo($studentId);
        $this->global['classes'] = $this->Class_model->getAllClassesByStudentId($studentId);
        $this->global['page'] = 'teacher/students';

        if ($classId != null && $schoolYearId != null) {
            $this->global['class'] = $this->Class_model->getStudentClassRelation($studentId, $classId, $schoolYearId);

            $this->form_validation->set_rules('sure', 'Ja', 'required|equalsToString[yes]');

            if (!$this->form_validation->run()) {
                $this->smarty->view('user/classSettings.tpl', $this->global);
            } else {
                $result = $this->Class_model->deleteStudentClassRelation($studentId, $classId, $schoolYearId);

                if ($result > 0) {
                    $this->session->set_flashdata('success_form', 'Klassenzuweisung erfolgreich gelöscht.');
                } else {
                    $this->session->set_flashdata('error_form', 'Klassenzuweisung konnte nicht gelöscht werden.');
                }

                redirect('teacher/students/class/' . $studentId);
            }
        } else {
            $this->smarty->view('user/classSettings.tpl', $this->global);
        }
    }

    function classSettingsAdd($studentId)
    {
        $this->global['title'] = 'Schüler Account Verwaltung - Klassenzuweisung';
        $this->global['user'] = $this->User_model->getUserInfo($studentId);
        $this->global['classes'] = $this->Class_model->getAllClassesByStudentId($studentId);
        $this->global['studentsDropdown'] = $this->User_model->getStudentAccounts();
        $this->global['classesDropdown'] = $this->Class_model->getAllClasses();
        $this->global['schoolYearsDropdown'] = $this->Schoolyear_model->getAllSchoolYears();
        $this->global['add'] = true;
        $this->global['page'] = 'teacher/students';

        $i = 0;
        $listClasses = "";
        $listSchoolYears = "";
        $lengthClasses = sizeof($this->global['classesDropdown']);
        $lengthSchoolYears = sizeof($this->global['schoolYearsDropdown']);

        // Classes List
        foreach ($this->global['classesDropdown'] as $class) {
            $listClasses = $listClasses . $class->id;
            $i++;
            if ($i != $lengthClasses) {
                $listClasses = $listClasses . ",";
            }
        }
        $i = 0;

        // SchoolYears List
        foreach ($this->global['schoolYearsDropdown'] as $schoolYear) {
            $listSchoolYears = $listSchoolYears . $schoolYear->id;
            $i++;
            if ($i != $lengthSchoolYears) {
                $listSchoolYears = $listSchoolYears . ",";
            }
        }

        $this->form_validation->set_rules('class', 'Klasse', 'required|in_list[' . $listClasses . ']|xss_clean');
        $this->form_validation->set_rules('schoolYear', 'Schuljahr', 'required|in_list[' . $listSchoolYears . ']|xss_clean');

        if (!$this->form_validation->run()) {
            $this->smarty->view('user/classSettings.tpl', $this->global);
        } else {
            $classId = $this->input->post('class');
            $schoolYearId = $this->input->post('schoolYear');

            $relationData = array(
                "studentID" => $studentId,
                "schoolyearID" => $schoolYearId,
                "classID" => $classId
            );

            $result = $this->Class_model->addStudentClassesRelation($relationData);

            if ($result > 0) {
                $this->session->set_flashdata('success_form', 'Klassenzuweisung erfolgreich hinzugefügt.');
            } else {
                $this->session->set_flashdata('error_form', 'Klassenzuweisung konnte nicht hinzugefügt werden.');
            }

            redirect('teacher/students/class/' . $studentId);
        }
    }
}