<?php

/**
 * Created by PhpStorm.
 * User: Lars-Laptop
 * Date: 24.05.2017
 * Time: 08:47
 */
class ClassManagement extends Base_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Class_model');
        $this->load->model('Schoolyear_model');
        $this->isLoggedIn();
        $this->hasAccess('teacher');
        $this->load->library('form_validation');
    }

    function overview()
    {
        $this->global['title'] = 'Klassen Übersicht';
        $this->global['classes'] = $this->Class_model->getAllClasses();
        $this->smarty->view('teacher/class.tpl', $this->global);
    }

    function create()
    {
        $this->global['title'] = 'Klassen - Hinzufügen';
        $this->form_validation->set_rules('name', 'Name', 'required|max_length[45]');
        $this->form_validation->set_rules('description', 'Beschreibung', 'required');

        if (!$this->form_validation->run()) {
            $this->smarty->view('teacher/class_add.tpl', $this->global);
        } else {
            $className = $this->input->post('name');
            $classDesc = $this->input->post('description');

            $result = $this->Class_model->checkIfClassNameAlreadyUsed($className);

            if (sizeof($result) == 0) {
                $classData = array(
                    "name" => $className,
                    "description" => $classDesc
                );

                $result = $this->Class_model->addClass($classData);

                if ($result > 0 || isset($password)) {
                    $this->session->set_flashdata('success_form', 'Klasse erfolgreich hinzugefügt.');
                } else {
                    $this->session->set_flashdata('error_form', 'Klasse konnte nicht hinzugefügt werden.');
                }
            } else {
                $this->session->set_flashdata('error_form', 'Klasse konnte nicht hinzugefügt werden, da bereits eine mit diesem Namen existiert.');
            }

            redirect("teacher/class");
        }
    }

    function edit($classId)
    {
        $this->global['title'] = 'Klassen - Bearbeiten';
        $this->global['class'] = $this->Class_model->getClassByID($classId);
        $this->form_validation->set_rules('name', 'Name', 'required|max_length[45]');
        $this->form_validation->set_rules('description', 'Beschreibung', 'required');

        if (!$this->form_validation->run()) {
            $this->smarty->view('teacher/class_edit.tpl', $this->global);
        } else {
            $className = $this->input->post('name');
            $classDesc = $this->input->post('description');

            $result = $this->Class_model->checkIfClassNameAlreadyUsed($className, $classId);

            if (sizeof($result) == 0) {
                $classData = array(
                    "name" => $className,
                    "description" => $classDesc
                );

                $result = $this->Class_model->updateClass($classId, $classData);

                if ($result > 0 || isset($password)) {
                    $this->session->set_flashdata('success_form', 'Klasse erfolgreich bearbeitet.');
                } else {
                    $this->session->set_flashdata('error_form', 'Klasse konnte nicht bearbeitet werden.');
                }
            } else {
                $this->session->set_flashdata('error_form', 'Klasse konnte nicht bearbeitet werden, da bereits eine mit diesem Namen existiert.');
            }

            redirect("teacher/class");
        }
    }

    function delete($classId)
    {
        $this->global['title'] = 'Lehrer Account Verwaltung - Löschen';
        $this->global['class'] = $this->Class_model->getClassByID($classId);

        $this->form_validation->set_rules('secret', 'Secret', 'required');

        if (!$this->form_validation->run()) {
            $this->smarty->view('teacher/class_delete.tpl', $this->global);
        } else {
            $secret = $this->input->post('secret');
            $className = $this->global['class'][0]->name;

            if ($secret == $className) {
                $result = $this->Class_model->deleteClass($classId);
                if ($result > 0) {
                    $this->session->set_flashdata('success_form', 'Klasse erfolgreich gelöscht.');
                } else {
                    $this->session->set_flashdata('error_form', 'Klasse konnte nicht gelöscht werden.');
                }
            } else {
                $this->session->set_flashdata('error_form', 'Der eingegebene Secret ist falsch.');
            }

            redirect("teacher/class");
        }
    }

    function classSchoolYearOverview($classId)
    {
        $this->global['class'] = $this->Class_model->getClassByID($classId);
        $this->global['schoolyear'] = $this->Schoolyear_model->getAllSchoolYears();

        foreach ($this->global['schoolyear'] as $var) {
            $this->global['classMemberCountBySchoolYear'][$var->id] = $this->Schoolyear_model->getClassMemberCountWithSchoolYear($classId, $var->id);
        }

        $this->global['title'] = 'Schüler Übersicht Klasse ' . $this->global['class'][0]->name;
        $this->smarty->view('teacher/class_member.tpl', $this->global);
    }

    function classMemberListWithSchoolYear($classId, $schoolYearId)
    {
        $this->global['class'] = $this->Class_model->getClassByID($classId);
        $this->global['classId'] = $classId;
        $this->global['schoolYearId'] = $schoolYearId;
        $this->global['schoolYear'] = $this->Schoolyear_model->getSchoolYearByID($schoolYearId);
        $this->global['classMember'] = $this->Schoolyear_model->getClassMemberWithSchoolYear($classId, $schoolYearId);
        $this->global['title'] = 'Schüler Übersicht Klasse ' . $this->global['class'][0]->name . 'Schuljahr ' . $this->global['schoolYear'][0]->year;
        $this->smarty->view('teacher/class_member_schoolyear_overview.tpl', $this->global);
    }
}