<?php

/**
 * Created by PhpStorm.
 * User: lars
 * Date: 23.05.17
 * Time: 11:13
 */
class Admin extends Base_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Subject_model');
        $this->isLoggedIn();
        $this->hasAccess('admin');
        $this->load->library('form_validation');
    }

    function teacher()
    {
        $this->global['title'] = 'Lehrer Account Verwaltung';
        $this->global['subtitle'] = 'Lehrer Accounts';
        $this->global['page'] = 'admin/teacher';
        $this->global['accounts'] = $this->User_model->getTeacherAccounts();
        $this->smarty->view('user/overview.tpl', $this->global);
    }

    function teacher_add()
    {
        $this->global['title'] = 'Lehrer Account Verwaltung - Hinzufügen';
        $this->global['page'] = 'admin/teacher';
        $this->form_validation->set_rules('firstname', 'Vorname', 'required|max_length[20]');
        $this->form_validation->set_rules('surename', 'Nachname', 'required|max_length[20]');
        $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
        $this->form_validation->set_rules('newPassword', 'Passwort', 'required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword', 'Passwort wiederholen', 'required|matches[newPassword]|max_length[20]');

        if (!$this->form_validation->run()) {
            $this->smarty->view('user/add.tpl', $this->global);
        } else {
            $firstName = $this->input->post('firstname');
            $sureName = $this->input->post('surename');
            $email = $this->input->post('email');
            $newPassword = $this->input->post('newPassword');

            $userData = array(
                "email" => $email,
                "password" => getHashedPassword($newPassword),
                "firstname" => $firstName,
                "surename" => $sureName
            );

            $result = $this->User_model->addNewUser($userData, 2);

            if ($result > 0 || isset($password)) {
                $this->session->set_flashdata('success_form', 'Lehrer erfolgreich hinzugefügt.');
            } else {
                $this->session->set_flashdata('error_form', 'Lehrer konnte nicht hinzugefügt werden.');
            }

            redirect("admin/teacher");
        }
    }

    function teacher_edit($userid)
    {
        $this->global['title'] = 'Lehrer Account Verwaltung - Bearbeiten';
        $this->global['user'] = $this->User_model->getUserInfo($userid);
        $this->global['page'] = 'admin/teacher';

        $this->form_validation->set_rules('firstname', 'Vorname', 'required|max_length[20]');
        $this->form_validation->set_rules('surename', 'Nachname', 'required|max_length[20]');
        $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
        $this->form_validation->set_rules('newPassword', 'Neues Passwort', 'max_length[20]');
        $this->form_validation->set_rules('cNewPassword', 'Neues Passwort wiederholen', 'matches[newPassword]|max_length[20]');

        if (!$this->form_validation->run()) {
            $this->smarty->view('user/edit.tpl', $this->global);
        } else {
            $firstname = $this->input->post('firstname');
            $surename = $this->input->post('surename');
            $email = $this->input->post('email');
            $newPassword = $this->input->post('newPassword');

            $userdata = array(
                "email" => $email,
                "firstname" => $firstname,
                "surename" => $surename
            );


            $result = $this->User_model->changeUserData($userid, $userdata);

            if ($newPassword != "") {
                $userdata = array(
                    "password" => getHashedPassword($newPassword)
                );
                $password = $this->User_model->changeUserData($userid, $userdata);
            }

            if ($result > 0 || isset($password)) {
                if (isset($password)) {
                    $this->session->set_flashdata('success_form', 'Lehrer erfolgreich aktualisiert. Das Passwort wurde geändert!');
                } else {
                    $this->session->set_flashdata('success_form', 'Lehrer erfolgreich aktualisiert.');
                }
            } else {
                $this->session->set_flashdata('error_form', 'Lehrer konnte nicht aktualisiert werden oder es wurden keine Änderung vorgenommen.');
            }

            redirect("admin/teacher/edit/$userid");
        }
    }

    function teacher_delete($userid)
    {
        $this->global['title'] = 'Lehrer Account Verwaltung - Löschen';
        $this->global['user'] = $this->User_model->getUserInfo($userid);
        $this->global['page'] = 'admin/teacher';

        $this->form_validation->set_rules('secret', 'Secret', 'required');

        if (!$this->form_validation->run()) {
            $this->smarty->view('user/delete.tpl', $this->global);
        } else {
            $secret = $this->input->post('secret');
            $fullname = $this->global['user'][0]->firstname . ' ' . $this->global['user'][0]->surename;

            if ($secret == $fullname) {
                $result = $this->User_model->deleteUser($userid);
                if ($result > 0) {
                    $this->session->set_flashdata('success_form', 'Lehrer erfolgreich gelöscht.');
                } else {
                    $this->session->set_flashdata('error_form', 'Lehrer konnte nicht gelöscht werden.');
                }
            } else {
                $this->session->set_flashdata('error_form', 'Der eingegebene Secret ist falsch.');
            }

            redirect("admin/teacher");
        }
    }

    function subjectList() {
        $this->global['title'] = 'Fächer Verwaltung';
        $this->global['subject'] = $this->Subject_model->getAllSubjects();
        $this->smarty->view('admin/subject.tpl', $this->global);
    }

    function subjectAdd() {
        $this->global['title'] = 'Fächer Verwaltung - Hinzufügen';
        $this->form_validation->set_rules('name', 'Name', 'required|max_length[45]');
        $this->form_validation->set_rules('shortname', 'Kurzname', 'required|max_length[5]');

        if (!$this->form_validation->run()) {
            $this->smarty->view('admin/subject_add.tpl', $this->global);
        } else {
            $subjectName = $this->input->post('name');
            $subjectShortName = $this->input->post('shortname');

            $classData = array(
                "name" => $subjectName,
                "shortname" => $subjectShortName
            );

            $result = $this->Subject_model->addSubject($classData);

            if ($result > 0 || isset($password)) {
                $this->session->set_flashdata('success_form', 'Fach erfolgreich hinzugefügt.');
            } else {
                $this->session->set_flashdata('error_form', 'Fach konnte nicht hinzugefügt werden.');
            }

            redirect("admin/subject");
        }
    }

    function subjectEdit($subjectId) {
        $this->global['title'] = 'Fächer Verwaltung - Bearbeiten';
        $this->global['subject'] = $this->Subject_model->getSubjectById($subjectId);
        $this->form_validation->set_rules('name', 'Name', 'required|max_length[45]');
        $this->form_validation->set_rules('shortname', 'Kurzname', 'required|max_length[5]');

        if (!$this->form_validation->run()) {
            $this->smarty->view('admin/subject_edit.tpl', $this->global);
        } else {
            $subjectName = $this->input->post('name');
            $subjectShortName = $this->input->post('shortname');

            $classData = array(
                "name" => $subjectName,
                "shortname" => $subjectShortName
            );

            $result = $this->Subject_model->updateSubject($subjectId, $classData);

            if ($result > 0 || isset($password)) {
                $this->session->set_flashdata('success_form', 'Fach erfolgreich bearbeitet.');
            } else {
                $this->session->set_flashdata('error_form', 'Fach konnte nicht bearbeitet werden.');
            }

            redirect("admin/subject");
        }
    }

    function subjectDelete($subjectId)
    {
        $this->global['title'] = 'Fächer Verwaltung - Löschen';
        $this->global['subject'] = $this->Subject_model->getSubjectById($subjectId);

        $this->form_validation->set_rules('secret', 'Secret', 'required');

        if (!$this->form_validation->run()) {
            $this->smarty->view('admin/subject_delete.tpl', $this->global);
        } else {
            $secret = $this->input->post('secret');
            $subjectName = $this->global['subject'][0]->name;

            if ($secret == $subjectName) {
                $result = $this->Subject_model->deleteSubject($subjectId);
                if ($result > 0) {
                    $this->session->set_flashdata('success_form', 'Fach erfolgreich gelöscht.');
                } else {
                    $this->session->set_flashdata('error_form', 'Fach konnte nicht gelöscht werden.');
                }
            } else {
                $this->session->set_flashdata('error_form', 'Der eingegebene Secret ist falsch.');
            }

            redirect("admin/subject");
        }
    }
}