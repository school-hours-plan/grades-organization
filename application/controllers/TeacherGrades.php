<?php

/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 03.06.2017
 * Time: 21:43
 */
class TeacherGrades extends Base_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Class_model');
        $this->load->model('Schoolyear_model');
        $this->load->model('Subject_model');
        $this->load->model('Grades_model');
        $this->load->model('Grades_value_model');
        $this->isLoggedIn();
        $this->hasAccess('teacher');
        $this->load->library('form_validation');
    }

    function gradesOverview($classId, $schoolYearId, $studentId)
    {
        $this->global['student'] = $this->User_model->getUserInfo($studentId);
        $this->global['class'] = $this->Class_model->getClassById($classId);
        $this->global['schoolYear'] = $this->Schoolyear_model->getSchoolYearByID($schoolYearId);
        $this->global['grades'] = $this->Grades_model->getGradesForOneStudent($studentId, $classId, $schoolYearId);
        $userTitle = $this->global['student'][0]->firstname . ' ' . $this->global['student'][0]->surename . ', ' . $this->global['class'][0]->name . ', ' . $this->global['schoolYear'][0]->year;
        $this->global['title'] = "Noten Verwaltung - Übersicht \"" . $userTitle . "\"";

        $this->smarty->view('teacher/grades/overview.tpl', $this->global);
    }

    function gradesAdd($classId, $schoolYearId, $studentId)
    {
        $this->global['title'] = "Noten Verwaltung - Erstellen";
        $this->global['classId'] = $classId;
        $this->global['schoolYearId'] = $schoolYearId;
        $this->global['studentId'] = $studentId;
        $this->global['StudentAccounts'] = $this->User_model->getStudentAccounts();
        $this->global['classes'] = $this->Class_model->getAllClasses();
        $this->global['schoolYears'] = $this->Schoolyear_model->getAllSchoolYears();
        $this->global['subjects'] = $this->Subject_model->getAllSubjects();
        $this->global['gradesValue'] = $this->Grades_value_model->getAllGradeValues();

        $listClasses = "";
        $listSchoolYears = "";
        $listStudents = "";
        $listSubjects = "";
        $listGrades = "";
        $lengthClasses = sizeof($this->global['classes']);
        $lengthSchoolYears = sizeof($this->global['schoolYears']);
        $lengthStudents = sizeof($this->global['StudentAccounts']);
        $lengthSubjects = sizeof($this->global['subjects']);
        $lengthGrades = sizeof($this->global['gradesValue']);
        $i = 0;

        // Classes List
        foreach ($this->global['classes'] as $class) {
            $listClasses = $listClasses . $class->id;
            $i++;
            if ($i != $lengthClasses) {
                $listClasses = $listClasses . ",";
            }
        }
        $i = 0;

        // SchoolYears List
        foreach ($this->global['schoolYears'] as $schoolYear) {
            $listSchoolYears = $listSchoolYears . $schoolYear->id;
            $i++;
            if ($i != $lengthSchoolYears) {
                $listSchoolYears = $listSchoolYears . ",";
            }
        }
        $i = 0;

        // Students List
        foreach ($this->global['StudentAccounts'] as $class) {
            $listStudents = $listStudents . $class->id;
            $i++;
            if ($i != $lengthStudents) {
                $listStudents = $listStudents . ",";
            }
        }
        $i = 0;

        // Subjects List
        foreach ($this->global['subjects'] as $subject) {
            $listSubjects = $listSubjects . $subject->id;
            $i++;
            if ($i != $lengthSubjects) {
                $listSubjects = $listSubjects . ",";
            }
        }

        // TeacherGrades List
        foreach ($this->global['gradesValue'] as $grade) {
            $listGrades = $listGrades . $grade->id;
            $i++;
            if ($i != $lengthGrades) {
                $listGrades = $listGrades . ",";
            }
        }

        $this->form_validation->set_rules('student', 'Schüler', 'required|in_list[' . $listStudents . ']|xss_clean');
        $this->form_validation->set_rules('class', 'Klasse', 'required|in_list[' . $listClasses . ']|xss_clean');
        $this->form_validation->set_rules('schoolYear', 'Schuljahr', 'required|in_list[' . $listSchoolYears . ']|xss_clean');
        $this->form_validation->set_rules('subject', 'Fach', 'required|in_list[' . $listSubjects . ']|xss_clean');
        $this->form_validation->set_rules('grade', 'Note', 'required|in_list[' . $listGrades . ']|xss_clean');

        if (!$this->form_validation->run()) {
            $this->smarty->view('teacher/grades/create.tpl', $this->global);
        } else {
            $studentIdForm = $this->input->post('student');
            $classIdForm = $this->input->post('class');
            $schoolYearIdForm = $this->input->post('schoolYear');
            $subjectId = $this->input->post('subject');
            $gradeId = $this->input->post('grade');

            $gradeData = array(
                "accountID" => $studentIdForm,
                "classID" => $classIdForm,
                "schoolyearID" => $schoolYearIdForm,
                "subjectID" => $subjectId,
                "teacherID" => $this->userId,
                "gradeID" => $gradeId,
                "timestamp" => time()
            );

            $result = $this->Grades_model->addGrade($gradeData);

            if ($result > 0) {
                $this->session->set_flashdata('success_form', 'Note erfolgreich hinzugefügt.');
            } else {
                $this->session->set_flashdata('error_form', 'Note konnte nicht hinzugefügt werden.');
            }

            redirect("teacher/class/grades/overview/" . $classId . "/" . $schoolYearId . "/" . $studentId);
        }

    }

    function gradesEdit($gradesId)
    {
        $this->global['StudentAccounts'] = $this->User_model->getStudentAccounts();
        $this->global['classes'] = $this->Class_model->getAllClasses();
        $this->global['schoolYears'] = $this->Schoolyear_model->getAllSchoolYears();
        $this->global['subjects'] = $this->Subject_model->getAllSubjects();
        $this->global['grade'] = $this->Grades_model->getGradeById($gradesId);
        $this->global['gradesValue'] = $this->Grades_value_model->getAllGradeValues();
        $this->global['title'] = "Noten Verwaltung - Bearbeiten";

        $i = 0;
        $listSubjects = "";
        $lengthSubjects = sizeof($this->global['subjects']);
        $listGrades = "";
        $lengthGrades = sizeof($this->global['gradesValue']);

        // Subject List
        foreach ($this->global['subjects'] as $subject) {
            $listSubjects = $listSubjects . $subject->id;
            $i++;
            if ($i != $lengthSubjects) {
                $listSubjects = $listSubjects . ",";
            }
        }

        // TeacherGrades List
        foreach ($this->global['gradesValue'] as $grade) {
            $listGrades = $listGrades . $grade->id;
            $i++;
            if ($i != $lengthGrades) {
                $listGrades = $listGrades . ",";
            }
        }

        $this->form_validation->set_rules('subject', 'Fach', 'required|in_list[' . $listSubjects . ']|xss_clean');
        $this->form_validation->set_rules('grade', 'Note', 'required|in_list[' . $listGrades . ']|xss_clean');

        if (!$this->form_validation->run()) {
            $this->smarty->view('teacher/grades/edit.tpl', $this->global);
        } else {
            $subjectId = $this->input->post('subject');
            $gradeId = $this->input->post('grade');

            $gradeData = array(
                "subjectID" => $subjectId,
                "gradeID" => $gradeId,
                "timestamp" => time()
            );

            $result = $this->Grades_model->updateGrade($gradesId, $gradeData);

            if ($result > 0) {
                $this->session->set_flashdata('success_form', 'Note erfolgreich bearbeitet.');
            } else {
                $this->session->set_flashdata('error_form', 'Note konnte nicht bearbeitet werden.');
            }

            redirect("teacher/class/grades/overview/" . $this->global['grade'][0]->classID . "/" . $this->global['grade'][0]->schoolyearID . "/" . $this->global['grade'][0]->accountID);
        }
    }

    function gradesDelete($gradesId)
    {
        $this->global['grade'] = $this->Grades_model->getGradeById($gradesId);
        $this->global['title'] = "Noten Verwaltung - Löschen";

        $this->form_validation->set_rules('deleteAccept', 'Secret', 'xss_clean');

        if (!$this->form_validation->run()) {
            $this->smarty->view('teacher/grades/delete.tpl', $this->global);
        } else {
            $deleteAccept = $this->input->post('deleteAccept');

            if (isset($deleteAccept)) {
                $this->Grades_model->deleteGrade($gradesId);
                $this->session->set_flashdata('success_form', 'Note erfolgreich gelöscht.');
            } else {
                $this->session->set_flashdata('error_form', 'Note wurde nicht gelöscht werden.');
            }

            redirect("teacher/class/grades/overview/" . $this->global['grade'][0]->classID . "/" . $this->global['grade'][0]->schoolyearID . "/" . $this->global['grade'][0]->accountID);
        }
    }
}