<?php

/**
 * Created by PhpStorm.
 * User: lars
 * Date: 13.06.17
 * Time: 12:22
 */
class Student extends Base_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Class_model');
        $this->load->model('Schoolyear_model');
        $this->load->model('Grades_model');
        $this->isLoggedIn();
        $this->hasAccess('student');
    }

    function classOverview()
    {
        $this->global['title'] = 'Noten Übersicht - Wähle eine Klasse';
        $this->global['classes'] = $this->Class_model->getAllClassesByStudentId($this->userId, true);

        $this->smarty->view('student/grades/classOverview.tpl', $this->global);
    }

    function schoolYearOverview($classId)
    {
        $this->global['title'] = 'Noten Übersicht - Wähle ein Schuljahr';
        $this->global['class'] = $this->Class_model->getClassById($classId);
        $this->global['schoolYears'] = $this->Schoolyear_model->getSchoolYearsByStudentAndClassId($this->userId, $classId);

        $this->smarty->view('student/grades/schoolYearOverview.tpl', $this->global);
    }

    function gradesOverview($classId, $schoolYearId)
    {
        $this->global['title'] = 'Noten Übersicht - Noten Übersicht';
        $this->global['class'] = $this->Class_model->getClassById($classId);
        $this->global['schoolYear'] = $this->Schoolyear_model->getSchoolYearByID($schoolYearId);
        $this->global['grades'] = $this->Grades_model->getGradesForOneStudent($this->userId, $classId, $schoolYearId);

        $this->smarty->view('student/grades/gradesOverview.tpl', $this->global);
    }
}