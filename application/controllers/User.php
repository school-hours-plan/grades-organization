<?php

/**
 * Created by PhpStorm.
 * User: lars
 * Date: 12.05.17
 * Time: 14:25
 */
class User extends Base_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Class_model');
        $this->load->model('Subject_model');
        $this->load->model('Grades_model');
        $this->isLoggedIn();
    }

    function index()
    {
        $this->global['title'] = 'Dashboard';
        $this->global['studentCount'] = $this->User_model->getStudentCount();
        $this->global['teacherCount'] = $this->User_model->getTeacherCount();
        $this->global['adminCount'] = $this->User_model->getAdminCount();
        $this->global['accountCount'] = $this->User_model->getUserCount();
        $this->global['classCount'] = $this->Class_model->getClassCount();
        $this->global['subjectCount'] = $this->Subject_model->getSubjectCount();
        $this->global['gradeCount'] = $this->Grades_model->getGradesCount();
        $this->global['personallyGradeCount'] = $this->Grades_model->getGradesCountByStudentId($this->userId);
        $this->global['personallyGradeAverage'] = $this->Grades_model->getGradesAverageByStudentId($this->userId);

        $this->smarty->view('dashboard.tpl', $this->global);
    }

    function settings()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('oldPassword', 'Altes Passowrt', 'required|max_length[20]');
        $this->form_validation->set_rules('newPassword', 'Neues Passwort', 'required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword', 'Neues Passwort wiederholen', 'required|matches[newPassword]|max_length[20]');

        if ($this->form_validation->run() == FALSE) {
            $this->global['title'] = 'Einstellungen';
            $this->smarty->view('settings.tpl', $this->global);
        } else {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');

            $resultPas = $this->User_model->matchOldPassword($this->userId, $oldPassword);

            if (empty($resultPas)) {
                $this->session->set_flashdata('error_form', 'Das eingegebene alte Passwort stimmt nicht überein.');
                redirect('user/settings');
            } else {
                $userdata = array(
                    "password" => getHashedPassword($newPassword)
                );
                $result = $this->User_model->changeUserData($this->userId, $userdata);

                if ($result > 0) {
                    $this->session->set_flashdata('success_form', 'Passwort erfolgreich aktualisiert.');
                } else {
                    $this->session->set_flashdata('error_form', 'Passwort konnte nicht aktualisiert werden.');
                }

                redirect('user/settings');
            }
        }
    }
}