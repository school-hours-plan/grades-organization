<?php

/**
 * Created by PhpStorm.
 * User: lars
 * Date: 23.05.17
 * Time: 12:43
 */
class Base_Form_validation extends CI_Form_validation
{
    function alpha_numeric_space($str)
    {
        return (!preg_match("/^([A-z0-9 ])+$/i", $str)) ? FALSE : TRUE;
    }

    function equalsToString($str, $param)
    {
        $this->set_message('equalsToString', 'Das Feld {field} hat nicht den Wert ' . $param . '!');
        return $str == $param ? TRUE : FALSE;
    }
}

