<?php

class Base_Controller extends CI_Controller
{
    protected $userId = '';
    protected $email = '';
    protected $firstname = '';
    protected $surename = '';
    protected $error_form = '';
    protected $success_form = '';
    protected $permissions = array();
    protected $global = array();

    function __construct()
    {
        parent::__construct();
        $this->load->model('Permission_model');

        $this->global['csrf'] = [
            'token_name' => $this->security->get_csrf_token_name(),
            'token_hash' => $this->security->get_csrf_hash()
        ];
    }

    /**
     * This function used to check the user is logged in or not
     */
    function isLoggedIn()
    {
        $isLoggedIn = $this->session->userdata('isLoggedIn');

        if (!isset ($isLoggedIn) || $isLoggedIn != TRUE) {
            redirect('login');
        } else {
            $this->userId = $this->session->userdata('userId');
            $this->email = $this->session->userdata('email');
            $this->firstname = $this->session->userdata('firstname');
            $this->surename = $this->session->userdata('surename');
            $this->error_form = $this->session->flashdata('error_form');
            $this->success_form = $this->session->flashdata('success_form');
            $this->permissions = $this->Permission_model->getSimpleAccountPermissionArray($this->userId);

            $this->global['userId'] = $this->userId;
            $this->global['firstname'] = $this->firstname;
            $this->global['surename'] = $this->surename;
            $this->global['error_form'] = $this->error_form;
            $this->global['success_form'] = $this->success_form;
            $this->global['permissions'] = $this->permissions;
        }
    }

    /**
     * This function will check if the current user has the expected permission
     * @param string $permission : expected Permission
     * @return bool permission_bool : User has/hasn't expected permission
     */
    function hasPermission($permission)
    {
        $bool = false;

        foreach ($this->permissions as $var) {
            if ($var == $permission) {
                $bool = true;
                break;
            }
        }

        return $bool;
    }

    /**
     * Checks if a user has the permission to access a specified site
     * @param string $permission : expected Permission
     */
    function hasAccess($permission)
    {
        $bool = $this->hasPermission($permission);
        if (!$bool) {
            redirect('/dashboard');
        }
    }

    /**
     * This function is used to logged out user from system
     */
    function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }
}