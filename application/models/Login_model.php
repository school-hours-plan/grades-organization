<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model
{
    /**
     * This function used to check the login credentials of the user
     * @param string $email : This is email of the user
     * @param string $password : This is encrypted password of the user
     * @return array userArray : The Array with the user informations
     */
    function loginMe($email, $password)
    {
        $this->db->select('id, password, email, firstname, surename');
        $this->db->from('accounts');
        $this->db->where('email', $email);
        $query = $this->db->get();
        $user = $query->result();

        if (!empty($user)) {
            if (verifyHashedPassword($password, $user[0]->password)) {
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    /**
     * This function used to check email exists or not
     * @param {string} $email : This is users email id
     * @return {boolean} $result : TRUE/FALSE
     */
    function checkEmailExist($email)
    {
        $this->db->select('id');
        $this->db->where('email', $email);
        $query = $this->db->get('accounts');

        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}