<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Lars-Laptop
 * Date: 18.05.2017
 * Time: 12:22
 */
class Permission_model extends CI_Model
{
    /**
     * @param int $userId : The ID of the user
     * @return mixed permission_array : An array with all user permissions
     */
    function getAccountPermissions($userId)
    {
        $this->db->select('permissions.permission_name');
        $this->db->from('accounts_permission');
        $this->db->join('permissions', 'permissions.id = accounts_permission.permissionID');
        $this->db->where('accounts_permission.accountID', $userId);
        $query = $this->db->get();
        $permission = $query->result();

        return $permission;
    }

    /**
     * @param int $userId : The ID of the user
     * @return array : A simple Array with all user permissions
     */
    function getSimpleAccountPermissionArray($userId)
    {
        $permissions = $this->getAccountPermissions($userId);
        $newPermissions = array();

        foreach ($permissions as $permission) {
            array_push($newPermissions, $permission->permission_name);
        }

        return $newPermissions;
    }

    /**
     * @return mixed permission_array : An array with all permissions
     */
    function getAllPermission()
    {
        $this->db->select('id, permission_name, permission_desc');
        $this->db->from('permissions');
        $query = $this->db->get();
        $permissions = $query->result();

        return $permissions;
    }

    /**
     * @return array : simple array with all permissions
     */
    function getSimplePermissionsArray()
    {
        $permissions = $this->getAllPermission();
        $newPermissions = array();

        foreach ($permissions as $permission) {
            array_push($newPermissions, $permission->permission_name);
        }

        return $newPermissions;
    }

    /**
     * @param integer $accountId : specified accountId
     * @return integer : number of affected rows in database
     */
    function removeAllAccountPermissions($accountId)
    {
        $this->db->where('accountID', $accountId);
        $this->db->delete('accounts_permission');

        return $this->db->affected_rows();
    }

    /**
     * @param array $permissionData : array with all expected data
     * @return integer : the id of the inserted data
     */
    function addAccountPermission($permissionData)
    {
        $this->db->trans_start();
        $this->db->insert('accounts_permission', $permissionData);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }
}