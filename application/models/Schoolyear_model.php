<?php

/**
 * Created by PhpStorm.
 * User: Lars-Laptop
 * Date: 31.05.2017
 * Time: 18:20
 */
class SchoolYear_model extends CI_Model
{
    /**
     * @return integer : count of SchoolYear entries
     */
    function getSchoolYearCount()
    {
        return $this->db->count_all_results('schoolyears');
    }

    /**
     * @return array : An array with all SchoolYears
     */
    function getAllSchoolYears()
    {
        $this->db->select('id, year');
        $this->db->from('schoolyears');
        $query = $this->db->get();
        $classes = $query->result();

        return $classes;
    }

    /**
     * @param integer $schoolYearId : Specified SchoolYear
     * @return array : An array with the SchoolYear Information
     */
    function getSchoolYearByID($schoolYearId)
    {
        $this->db->select('id, year');
        $this->db->from('schoolyears');
        $this->db->where('id', $schoolYearId);

        return $this->db->get()->result();
    }

    /**
     * @param integer $classId : Specified Class
     * @param integer $schoolYearId : Specified SchoolYear
     * @return int : the count of class member in the specified class of the specified SchoolYear
     */
    function getClassMemberCountWithSchoolYear($classId, $schoolYearId)
    {
        $this->db->select('*');
        $this->db->from('schoolyears');
        $this->db->where('schoolyears.id', $schoolYearId);
        $this->db->join('students_classes', 'students_classes.schoolYearId = schoolyears.id AND students_classes.classId = ' . $classId);

        return $this->db->get()->num_rows();
    }

    /**
     * @param integer $classId : Specified Class
     * @param integer $schoolYearId : Specified SchoolYear
     * @return array : array with all Class Member with the specified SchoolYear
     */
    function getClassMemberWithSchoolYear($classId, $schoolYearId)
    {
        $this->db->select('accounts.id, accounts.email, accounts.firstname, accounts.surename');
        $this->db->from('schoolyears');
        $this->db->where('schoolyears.id', $schoolYearId);
        $this->db->join('students_classes', 'students_classes.schoolYearId = schoolyears.id AND students_classes.classId = ' . $classId);
        $this->db->join('accounts', 'accounts.id = students_classes.studentId');

        return $this->db->get()->result();
    }

    function getSchoolYearsByStudentAndClassId($studentId, $classId)
    {
        $this->db->select('*');
        $this->db->from('students_classes as sc');
        $this->db->join('classes as cl', 'sc.classID = cl.id');
        $this->db->join('schoolyears as sy', 'sc.schoolyearID = sy.id');
        $this->db->where('sc.studentID', $studentId);
        $this->db->where('sc.classID', $classId);

        return $this->db->get()->result();
    }
}