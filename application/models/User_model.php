<?php

class User_model extends CI_Model
{
    /**
     * this functions returns the numbers of rows in the accounts table
     * @return integer
     */
    function getUserCount()
    {
        return $this->db->count_all_results('accounts');
    }

    /**
     * @return integer
     */
    function getStudentCount()
    {
        $this->db->select('*');
        $this->db->from('accounts as ac');
        $this->db->join('accounts_permission as acp', 'ac.id = acp.accountID');
        $this->db->where('acp.permissionID', 1);

        return $this->db->get()->num_rows();
    }

    /**
     * @return integer
     */
    function getTeacherCount()
    {
        $this->db->select('*');
        $this->db->from('accounts as ac');
        $this->db->join('accounts_permission as acp', 'ac.id = acp.accountID');
        $this->db->where('acp.permissionID', 2);

        return $this->db->get()->num_rows();
    }

    /**
     * @return integer
     */
    function getAdminCount()
    {
        $this->db->select('*');
        $this->db->from('accounts as ac');
        $this->db->join('accounts_permission as acp', 'ac.id = acp.accountID');
        $this->db->where('acp.permissionID', 4);

        return $this->db->get()->num_rows();
    }

    /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getUserInfo($userId)
    {
        $this->db->select('id, email, firstname, surename');
        $this->db->from('accounts');
        $this->db->where('id', $userId);
        $query = $this->db->get();

        return $query->result();
    }

    /**
     * This function is used to add new user to system
     * @param array $userInfo : An Array with the user informations
     * @param integer $permissionID : The ID as Integer of the Permission which will be granted to the new user
     * @return number $insert_id : This is last inserted id
     */
    function addNewUser($userInfo, $permissionID = null)
    {
        $this->db->trans_start();
        $this->db->insert('accounts', $userInfo);

        $insert_id = $this->db->insert_id();

        if ($permissionID != null) {
            $permissionInfo = array(
                "accountID" => $insert_id,
                "permissionID" => $permissionID
            );

            $this->db->insert('accounts_permission', $permissionInfo);
        }

        $this->db->trans_complete();

        return $insert_id;
    }

    /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     * @return boolean TRUE : always returns true
     */
    function editUser($userInfo, $userId)
    {
        $this->db->where('id', $userId);
        $this->db->update('accounts', $userInfo);

        return TRUE;
    }

    /**
     * This function is used to delete the user information
     * @param number $userId : This is user id
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser($userId)
    {
        $this->db->where('accountID', $userId);
        $this->db->delete('accounts_permission');

        $this->db->where('id', $userId);
        $this->db->delete('accounts');

        return $this->db->affected_rows();
    }

    /**
     * This function is used to check whether email id is already exist or not
     * @param string $email : This is email id
     * @param integer $userId : This is user id
     * @return mixed $result : This is searched result
     */
    function checkEmailExists($email, $userId = 0)
    {
        $this->db->select("email");
        $this->db->from("accounts");
        $this->db->where("email", $email);
        if ($userId != 0) {
            $this->db->where("id !=", $userId);
        }
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * This function is used to match users password for change password
     * @param number $userId : This is user id
     * @return array $result : This is searched result
     */
    function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('id, password');
        $this->db->where('id', $userId);
        $query = $this->db->get('accounts');

        $user = $query->result();
        if (!empty($user)) {
            if (verifyHashedPassword($oldPassword, $user[0]->password)) {
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    /**
     * This function is used to change users data
     * @param number $userId : This is user id
     * @param array $userInfo : This is user updation info
     */
    function changeUserData($userId, $userInfo)
    {
        $this->db->where('id', $userId);
        $this->db->update('accounts', $userInfo);

        return $this->db->affected_rows();
    }

    /**
     * @return array : An array with all Accounts
     */
    function getAllAccounts()
    {
        $this->db->select('accounts.id, accounts.email, accounts.firstname, accounts.surename');
        $this->db->from('accounts');
        $query = $this->db->get();

        return $query->result();
    }

    /**
     * @return array : An array with all Student Accounts
     */
    function getStudentAccounts()
    {
        $this->db->select('accounts.id, accounts.email, accounts.firstname, accounts.surename, accounts_permission.permissionID');
        $this->db->from('accounts');
        $this->db->join('accounts_permission', 'accounts.id = accounts_permission.accountID');
        $this->db->where('accounts_permission.permissionID', 1);
        $query = $this->db->get();

        return $query->result();
    }

    /**
     * @return array : An array with all Teacher Accounts
     */
    function getTeacherAccounts()
    {
        $this->db->select('accounts.id, accounts.email, accounts.firstname, accounts.surename, accounts_permission.permissionID');
        $this->db->from('accounts');
        $this->db->join('accounts_permission', 'accounts.id = accounts_permission.accountID');
        $this->db->where('accounts_permission.permissionID', 2);
        $query = $this->db->get();

        return $query->result();
    }

    /**
     * @param integer $classId : The id of the class
     * @param integer $schoolYearId : The id of the schoolyear
     * @return array : An array with all Users by classid
     */
    function getUserByClassAndSchoolYearId($classId, $schoolYearId)
    {
        $this->db->select('accounts.id, accounts.email, accounts.firstname, accounts.surename');
        $this->db->from('accounts');
        $this->db->join('students_classes', 'accounts.id = students_classes.studentID');
        $this->db->where('students_classes.classID', $classId);
        $this->db->where('students_classes.schoolyearID', $schoolYearId);

        return $this->db->get()->result();
    }
}