<?php

/**
 * Created by PhpStorm.
 * User: lars
 * Date: 09.06.17
 * Time: 11:35
 */
class Grades_value_model extends CI_Model
{
    /**
     * @return array : all grade values
     */
    function getAllGradeValues() {
        $this->db->select('*');
        $this->db->from('grades_value');

        return $this->db->get()->result();
    }
}