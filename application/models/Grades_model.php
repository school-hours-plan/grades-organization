<?php

/**
 * Created by PhpStorm.
 * User: Lars
 * Date: 03.06.2017
 * Time: 21:54
 */
class Grades_model extends CI_Model
{

    /**
     * @return int
     */
    function getGradesCount()
    {
        return $this->db->count_all_results('grades');
    }

    /**
     * @param integer $studentId : id of the specified student
     * @return integer
     */
    function getGradesCountByStudentId($studentId)
    {
        $this->db->select('*');
        $this->db->from('grades');
        $this->db->where('accountID', $studentId);

        return $this->db->get()->num_rows();
    }

    /**
     * @param integer $studentId : id of the specified student
     * @return int
     */
    function getGradesAverageByStudentId($studentId)
    {
        $this->db->select('*');
        $this->db->from('grades');
        $this->db->where('accountID', $studentId);
        $this->db->join('grades_value as gv', 'grades.gradeID = gv.id');
        $grades = $this->db->get()->result();

        $avg = 0;
        $i = 0;

        foreach ($grades as $gr) {
            $avg += $gr->number;
            $i++;
        }
        $avg /= $i;

        return $avg;
    }

    /**
     * @param int $studentId : the specified id of the student
     * @param int $classId : the specified id of the class
     * @param int $schoolYearId : the specified id of the schoolyear
     * @return array : An array with all information of the grades
     */
    function getGradesForOneStudent($studentId, $classId, $schoolYearId)
    {
        $this->db->select('gr.id, gr.timestamp, grv.name AS gradeName, su.name AS subjectName, su.shortname AS subjectShortname, teacher.firstname AS teacherFirstname, teacher.surename AS teacherSurename');
        $this->db->from('grades AS gr');
        $this->db->where('accountID', $studentId);
        $this->db->where('classID', $classId);
        $this->db->where('schoolyearID', $schoolYearId);
        $this->db->join('subjects AS su', 'gr.subjectID = su.id');
        $this->db->join('accounts AS teacher', 'gr.teacherID = teacher.id');
        $this->db->join('grades_value AS grv', 'gr.gradeID = grv.id');

        return $this->db->get()->result();
    }

    /**
     * @param int $gradeId : the specified id of the grade
     * @return array : An array with all information of the grade
     */
    function getGradeById($gradeId)
    {
        $this->db->select('gr.*');
        $this->db->from('grades AS gr');
        $this->db->where('gr.id', $gradeId);

        return $this->db->get()->result();
    }

    /**
     * @param array $gradeData : the grade data which will be inserted
     * @return integer : the id of the inserted
     */
    function addGrade($gradeData)
    {
        $this->db->trans_start();
        $this->db->insert('grades', $gradeData);
        $insertID = $this->db->insert_id();
        $this->db->trans_complete();
        return $insertID;
    }

    /**
     * This function is used to update the user information
     * @param number $gradeId : This is grade id
     * @param array $gradeData : This is grade updated information
     * @return boolean TRUE : always returns true
     */
    function updateGrade($gradeId, $gradeData)
    {
        $this->db->where('id', $gradeId);
        $this->db->update('grades', $gradeData);

        return TRUE;
    }

    /**
     * This function is used to delete the user information
     * @param number $gradeId : This is the id of the grade
     * @return int : the number of affected rows
     */
    function deleteGrade($gradeId)
    {
        $this->db->where('id', $gradeId);
        $this->db->delete('grades');

        return $this->db->affected_rows();
    }
}