<?php

/**
 * Created by PhpStorm.
 * User: Lars-Laptop
 * Date: 24.05.2017
 * Time: 08:50
 */
class Class_model extends CI_Model
{

    /**
     * @return integer
     */
    function getClassCount()
    {
        return $this->db->count_all_results('classes');
    }

    /**
     * @return array : An array with all classes
     */
    function getAllClasses()
    {
        $this->db->select('id, name, description');
        $this->db->from('classes');
        $query = $this->db->get();
        $classes = $query->result();

        return $classes;
    }

    /**
     * @param integer $studentId : specified student
     * @return array : array with all classes found by student id
     */
    function getAllClassesByStudentId($studentId, $groupBy = false)
    {
        $this->db->select('cl.name, cl.description, sc.classID, sc.schoolyearID, sy.year');
        $this->db->from('students_classes as sc');
        $this->db->join('classes as cl', 'sc.classID = cl.id');
        $this->db->join('schoolyears as sy', 'sc.schoolyearID = sy.id');
        $this->db->where('sc.studentID', $studentId);
        if ($groupBy) {
            $this->db->group_by('sc.classID');
        }

        return $this->db->get()->result();
    }

    /**
     * @param integer $classId : Specified class
     * @return array : An array with the class
     */
    function getClassByID($classId)
    {
        $this->db->select('id, name, description');
        $this->db->from('classes');
        $this->db->where('id', $classId);

        return $this->db->get()->result();
    }

    /**
     * @param $studentId
     * @param $classId
     * @param $schoolYearId
     * @return array
     */
    function getStudentClassRelation($studentId, $classId, $schoolYearId)
    {
        $this->db->select('sc.studentID, sc.schoolyearID, sc.classID, cl.name, cl.description, sy.year');
        $this->db->from('students_classes as sc');
        $this->db->join('classes as cl', 'sc.classID = cl.id');
        $this->db->join('schoolyears as sy', 'sc.schoolyearID = sy.id');
        $this->db->where('sc.studentID', $studentId);
        $this->db->where('sc.classID', $classId);
        $this->db->where('sc.schoolyearID', $schoolYearId);

        return $this->db->get()->result();
    }

    /**
     * @param $studentId
     * @param $classId
     * @param $schoolYearId
     * @return integer : number of affected rows
     */
    function deleteStudentClassRelation($studentId, $classId, $schoolYearId)
    {
        $this->db->where('studentID', $studentId);
        $this->db->where('classID', $classId);
        $this->db->where('schoolyearID', $schoolYearId);
        $this->db->delete('students_classes');

        return $this->db->affected_rows();
    }

    /**
     * @param integer $studentId : specified user
     * @return array : simple array with all class ids
     */
    function getSimpleClassesByUserIdArray($studentId)
    {
        $classes = $this->getAllClassesByStudentId($studentId);
        $newClasses = array();

        foreach ($classes as $class) {
            array_push($newClasses, $class->id);
        }

        return $newClasses;
    }

    /**
     * @param array $relationData : array with all needed relation data
     * @return integer : the id of the inserted relation data
     */
    function addStudentClassesRelation($relationData)
    {
        $this->db->trans_start();
        $this->db->insert('students_classes', $relationData);
        $this->db->trans_complete();

        return 1;
    }

    /**
     * @param array $classData : the class data which will be inserted
     * @return integer : the id of the inserted
     */
    function addClass($classData)
    {
        $this->db->trans_start();
        $this->db->insert('classes', $classData);
        $insertID = $this->db->insert_id();
        $this->db->trans_complete();
        return $insertID;
    }

    /**
     * This function is used to update the user information
     * @param number $classId : This is class id
     * @param array $classData : This is class updated information
     * @return boolean TRUE : always returns true
     */
    function updateClass($classId, $classData)
    {
        $this->db->where('id', $classId);
        $this->db->update('classes', $classData);

        return TRUE;
    }

    /**
     * This function is used to delete the user information
     * @param number $classId : This is class id
     * @return int : the number of affected rows
     */
    function deleteClass($classId)
    {
        $this->db->where('id', $classId);
        $this->db->delete('classes');

        return $this->db->affected_rows();
    }

    /**
     * @param string $className : the name of the class
     * @param integer $classId : can be null but when not, the specified classId will be except of the query result
     * @return array : array with classes with the same name
     */
    function checkIfClassNameAlreadyUsed($className, $classId = null)
    {
        $this->db->select('*');
        $this->db->from('classes');
        $this->db->where('name', $className);

        if ($classId != null) {
            $this->db->where('id !=', $classId);
        }

        return $this->db->get()->result();
    }
}