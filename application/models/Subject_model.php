<?php

/**
 * Created by PhpStorm.
 * User: Lars-Laptop
 * Date: 01.06.2017
 * Time: 09:58
 */
class Subject_model extends CI_Model
{

    /**
     * @return integer
     */
    function getSubjectCount()
    {
        return $this->db->count_all_results('subjects');
    }

    /**
     * @return array : All Subjects
     */
    function getAllSubjects()
    {
        $this->db->select('*');
        $this->db->from('subjects');

        return $this->db->get()->result();
    }

    function getSubjectById($subjectId)
    {
        $this->db->select('*');
        $this->db->from('subjects');
        $this->db->where('id', $subjectId);

        return $this->db->get()->result();
    }

    function addSubject($subjectData)
    {
        $this->db->trans_start();
        $this->db->insert('subjects', $subjectData);
        $insertID = $this->db->insert_id();
        $this->db->trans_complete();

        return $insertID;
    }

    function updateSubject($subjectId, $subjectData)
    {
        $this->db->where('id', $subjectId);
        $this->db->update('subjects', $subjectData);

        return TRUE;
    }

    function deleteSubject($subjectId)
    {
        $this->db->where('id', $subjectId);
        $this->db->delete('subjects');

        return $this->db->affected_rows();
    }
}
