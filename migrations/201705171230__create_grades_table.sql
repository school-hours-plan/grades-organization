CREATE TABLE IF NOT EXISTS `customer186_grades`.`subjects` (
  `id`        INT         NOT NULL AUTO_INCREMENT,
  `name`      VARCHAR(45) NOT NULL,
  `shortname` VARCHAR(5)  NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `customer186_grades`.`schoolyears` (
  `id`   INT         NOT NULL AUTO_INCREMENT,
  `year` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `customer186_grades`.`grades_value` (
  `id`     INT         NOT NULL AUTO_INCREMENT,
  `name`   VARCHAR(45) NOT NULL,
  `number` INT         NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `customer186_grades`.`classes` (
  `id`          INT         NOT NULL AUTO_INCREMENT,
  `name`        VARCHAR(45) NOT NULL,
  `description` TEXT        NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `customer186_grades`.`students_classes` (
  `studentID`    INT NOT NULL,
  `schoolyearID` INT NOT NULL,
  `classID`      INT NOT NULL,
  INDEX `fk_students_classes_studentid_idx` (`studentID` ASC),
  INDEX `fk_students_classes_schoolyearid_idx` (`schoolyearID` ASC),
  INDEX `fk_students_classes_classid_idx` (`classID` ASC),
  PRIMARY KEY (`studentID`, `schoolyearID`, `classID`),
  CONSTRAINT `fk_students_classes_studentid`
  FOREIGN KEY (`studentID`)
  REFERENCES `customer186_grades`.`accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_students_classes_schoolyearid`
  FOREIGN KEY (`schoolyearID`)
  REFERENCES `customer186_grades`.`schoolyears` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_students_classes_classid`
  FOREIGN KEY (`classID`)
  REFERENCES `customer186_grades`.`classes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `customer186_grades`.`grades` (
  `id`           INT NOT NULL AUTO_INCREMENT,
  `accountID`    INT NOT NULL,
  `classID`      INT NOT NULL,
  `schoolyearID` INT NOT NULL,
  `subjectID`    INT NOT NULL,
  `teacherID`    INT NOT NULL,
  `gradeID`      INT NOT NULL,
  `timestamp`    INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_grades_accountid_idx` (`accountID` ASC),
  INDEX `fk_grades_schoolyearid_idx` (`schoolyearID` ASC),
  INDEX `fk_grades_subjectid_idx` (`subjectID` ASC),
  INDEX `fk_grades_classid_idx` (`classID` ASC),
  INDEX `fk_grades_teacherid_idx` (`teacherID` ASC),
  INDEX `fk_grades_gradeid_idx` (`gradeID` ASC),
  CONSTRAINT `fk_grades_accountid`
  FOREIGN KEY (`accountID`)
  REFERENCES `customer186_grades`.`accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_grades_schoolyearid`
  FOREIGN KEY (`schoolyearID`)
  REFERENCES `customer186_grades`.`schoolyears` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_grades_subjectid`
  FOREIGN KEY (`subjectID`)
  REFERENCES `customer186_grades`.`subjects` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_grades_classid`
  FOREIGN KEY (`classID`)
  REFERENCES `customer186_grades`.`classes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_grades_teacherid`
  FOREIGN KEY (`teacherID`)
  REFERENCES `customer186_grades`.`accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_grades_gradeid`
  FOREIGN KEY (`gradeID`)
  REFERENCES `customer186_grades`.`grades_value` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB;

-- Inserts
INSERT INTO `customer186_grades`.`schoolyears` (`id`, `year`) VALUES (1, '15/16');
INSERT INTO `customer186_grades`.`schoolyears` (`id`, `year`) VALUES (2, '16/17');
INSERT INTO `customer186_grades`.`schoolyears` (`id`, `year`) VALUES (3, '17/18');

INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (1, 'voll Sehr gut (+)', 1);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (2, 'Sehr gut', 1);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (3, 'knapp sehr gut (-)', 1);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (4, 'voll Gut (+)', 2);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (5, 'Gut', 2);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (6, 'knapp Gut (-)', 2);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (7, 'voll Befriedigend (+)', 3);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (8, 'Befriedigend', 3);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (9, 'knapp Befriedigend (-)', 3);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (10, 'voll Ausreichend (+)', 4);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (11, 'Ausreichend', 4);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (12, 'knapp Ausreichend (-)', 4);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (13, 'knapp Mangelhaft (+)', 5);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (14, 'Mangelhaft', 5);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (15, 'voll Mangelhaft (-)', 5);
INSERT INTO `customer186_grades`.`grades_value` (`id`, `name`, `number`) VALUES (16, 'Ungenügend', 6);