CREATE TABLE IF NOT EXISTS `grades_session` (
        `id` varchar(128) NOT NULL,
        `ip_address` varchar(45) NOT NULL,
        `timestamp` int(10) unsigned DEFAULT 0 NOT NULL,
        `data` blob NOT NULL,
        KEY `grades_session_timestamp` (`timestamp`)
);
