CREATE TABLE IF NOT EXISTS `customer186_grades`.`accounts` (
  `id`        INT  NOT NULL AUTO_INCREMENT,
  `email`     TEXT NOT NULL,
  `password`  TEXT NOT NULL,
  `firstname` TEXT NOT NULL,
  `surename`  TEXT NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB;