INSERT INTO `customer186_grades`.`permissions` (`id`, `permission_name`, `permission_desc`) VALUES (DEFAULT, 'student', 'Schüler');
INSERT INTO `customer186_grades`.`permissions` (`id`, `permission_name`, `permission_desc`) VALUES (DEFAULT, 'teacher', 'Lehrer');
INSERT INTO `customer186_grades`.`permissions` (`id`, `permission_name`, `permission_desc`) VALUES (DEFAULT, 'teacher_admin', 'Lehrer (Admin)');
INSERT INTO `customer186_grades`.`permissions` (`id`, `permission_name`, `permission_desc`) VALUES (DEFAULT, 'admin', 'Admin');