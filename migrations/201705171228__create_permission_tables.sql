CREATE TABLE IF NOT EXISTS `customer186_grades`.`permissions` (
  `id`              INT         NOT NULL AUTO_INCREMENT,
  `permission_name` VARCHAR(45) NOT NULL,
  `permission_desc` TEXT        NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `customer186_grades`.`accounts_permission` (
  `id`           INT NOT NULL AUTO_INCREMENT,
  `accountID`    INT NOT NULL,
  `permissionID` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_accounts_permission_permissionid_idx` (`permissionID` ASC),
  INDEX `fk_accounts_permission_accountid_idx` (`accountID` ASC),
  CONSTRAINT `fk_accounts_permission_accountid`
  FOREIGN KEY (`accountID`)
  REFERENCES `customer186_grades`.`accounts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_accounts_permission_permissionid`
  FOREIGN KEY (`permissionID`)
  REFERENCES `customer186_grades`.`permissions` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB;